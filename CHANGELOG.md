# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.4

- patch: Updated readme

## 0.2.3

- patch: Updated readme

## 0.2.2

- patch: Build URL

## 0.2.1

- patch: Enable Debug for JFrog CLI

## 0.2.0

- minor: Minor Release

## 0.1.0

- minor: Minor Release

## 0.0.1

- patch: Initial release

