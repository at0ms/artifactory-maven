#!/bin/bash
#
# artifactory-maven pipe
#
# Required globals:
#   ARTIFACTORY_URL
#   ARTIFACTORY_USER
#   ARTIFACTORY_PASSWORD
#   MAVEN_SNAPSHOT_REPO
#   MAVEN_RELEASE_REPO
#
# Optional globals:
#   FOLDER
#   EXTRA_ARGS
#   MAVEN_COMMAND
#   JFROG_CLI_TEMP_DIR
#   JFROG_CLI_HOME_DIR
#   BUILD_NAME
#

source "$(dirname "$0")/common.sh"

## Enable debug mode.
DEBUG_ARGS=
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    DEBUG_ARGS="--verbose"
    export JFROG_CLI_LOG_LEVEL="DEBUG"
  fi
}

info "Starting pipe execution..."

# required parameters
ARTIFACTORY_URL=${ARTIFACTORY_URL:?'ARTIFACTORY_URL environment variable missing.'}
ARTIFACTORY_USER=${ARTIFACTORY_USER:?'ARTIFACTORY_USER environment variable missing.'}
ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD:?'ARTIFACTORY_PASSWORD environment variable missing.'}
MAVEN_SNAPSHOT_REPO=${MAVEN_SNAPSHOT_REPO:?'MAVEN_SNAPSHOT_REPO environment variable missing.'}
MAVEN_RELEASE_REPO=${MAVEN_RELEASE_REPO:?'MAVEN_RELEASE_REPO environment variable missing.'}

# optional parameters
MAVEN_COMMAND=${MAVEN_COMMAND:="clean install"}
BUILD_NAME=${BUILD_NAME:=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH}
FOLDER=${FOLDER:="."}
JFROG_CLI_TEMP_DIR=${JFROG_CLI_TEMP_DIR:="${FOLDER}/"}
JFROG_CLI_HOME_DIR=${JFROG_CLI_HOME_DIR:="${FOLDER}/"}
COLLECT_ENV=${COLLECT_ENV:="true"}
COLLECT_BUILD_INFO=${COLLECT_BUILD_INFO:="true"}
EXTRA_ARGS=${EXTRA_ARGS:=""}
DEBUG=${DEBUG:="false"}

# Set the environment variable
export M2_HOME=/usr/share/maven
export JFROG_CLI_TEMP_DIR=$JFROG_CLI_TEMP_DIR
export JFROG_CLI_HOME_DIR=$JFROG_CLI_HOME_DIR
export BUILD_URL="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"

cat <<EOF >$FOLDER/configuration.yaml
version: 1
type: maven
resolver:
  snapshotRepo: ${MAVEN_SNAPSHOT_REPO}
  releaseRepo: ${MAVEN_RELEASE_REPO}
  serverID: artifactory
deployer:
  snapshotRepo: ${MAVEN_SNAPSHOT_REPO}
  releaseRepo: ${MAVEN_RELEASE_REPO}
  serverID: artifactory
EOF

debug "build name is ${BUILD_NAME}"
debug "build number is ${BITBUCKET_BUILD_NUMBER}"

# Configure Artifactory instance with JFrog CLI
run jfrog rt config --url=$ARTIFACTORY_URL --user=$ARTIFACTORY_USER --password=$ARTIFACTORY_PASSWORD --interactive=false artifactory $EXTRA_ARGS

# Run the MVN install command
run jfrog rt mvn "${MAVEN_COMMAND} -f ${FOLDER}/pom.xml" $FOLDER/configuration.yaml --build-name=$BUILD_NAME --build-number=$BITBUCKET_BUILD_NUMBER $EXTRA_ARGS

# Capture environment variables for build information
if [[ "${COLLECT_ENV}" == "true" ]]; then
   info "Capturing environment variables"
   run jfrog rt bce $BUILD_NAME $BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
fi

# Publish build information to Artifactory
if [[ "${COLLECT_BUILD_INFO}" == "true" ]]; then
   info "Capturing build information"
   run jfrog rt bp $BUILD_NAME $BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
fi

if [[ "${status}" -eq 0 ]]; then
  success "Maven packages published successfully."
else
  fail "Failed to publish Maven packages."
fi
